# Travactory N.V. - Java Development Test

Your goal is to implement a **flight aggregation service** for an online travel agency. This component will be used for listing all available flights when preparing a broader holiday offer for the customer (eg. flights "from Amsterdam to Barcelona on 01 April 2015" could be presented as an addition to a hotel booking made in Barcelona on that date).

In this scenario we have two suppliers (lowcost & regular flight supplier), that provide flight definitions in slightly different format. Both are accessed using a Web Service, which is represented as a Java stub in an isolated package (LowcostFlightSupplierWS & RegularFlightSupplierWS). **Your task is to implement a new service that will aggregate results from both services and expose them as a sequence of objects in a common format**.

# Rules

1. Assume that this module will be integrated into some sort of server environment (eg. a web app implemented in Spring, a JEE app or similar). Please feel free to pick one and provide a way to integrate your module with that environment.
2. The template is written in Java. However, you're free to use any language, that runs on the JVM (as long as it works with the stubs provided in the template).
3. Feel free to use any libraries from the [Maven Central Repository](http://search.maven.org/). Basic Maven and Gradle configs are at your disposal.
4. Please assume production quality during implementation (e.g. write some tests). Generally speaking, we should be able to demo this module using the provided Main class, a complete integration test or both.
5. Your commit log should reflect real life development process (i.e. make often commits, write some comments etc.).
6. You can alter or remove anything, apart from LowcostFlightSupplierWS and RegularFlightSupplierWS.

# Design guidelines

This is an "open" assignment, which can be implemented in multiple ways and with different complexity in mind. What we're particularly interested in is how you approach a real life business problem from a development perspective. Please consider the following when working on this assignment:

1. Business usability (e.g. the contract of the service should be clear and extensible)
2. Design patterns
3. Performance considerations
4. Error handling, maintainability & general code quality

# Submission instructions

1. Please fork the [project](https://bitbucket.org/Travactory/java-recruitment-app).
2. Implement your solution to the problem.
3. Give READ access (Settings -> Access management) to your forked repository to Travactory. 

Good luck!

